# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## [Unreleased]

### Changed

- test: use personal docker registry

## v2.0.0 - 2021-08-20

### Breaked

- use new variable postfix_config

### Added

- test: add suport debian 10 and 11
- feat: add bsd-mailx package
- feat: add transport map
- feat: add aliases

### Changed

- chore: use FQCN for module name
- test: replace kitchen to molecule

### Removed

- test: remove support debian 9

## v1.0.0 - 2019-03-07

- first version
