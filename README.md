# Ansible role: Postfix MTA

[![Version](https://img.shields.io/badge/latest_version-2.0.0-green.svg)](https://code.waks.be/nishiki/ansible-role-postfix_mta/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-postfix_mta/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-postfix_mta/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-postfix_mta/actions?workflow=molecule.yml)

Install and configure an simple mta with postfix

## Requirements

- Ansible >= 2.9
- Debian
  - Bullseye
  - Bookworm

## Role variables

- `postfix_config` - hash with config

```
  smtpd_banner: $myhostname ESMTP $mail_name (Debian/GNU)
  biff: 'no'
  append_dot_mydomain: 'no'
  readme_directory: 'no'
  compatibility_level: 2
  myhostname: '{{ ansible_fqdn }}'
  myorigin: $myhostname
  mydestination: $myhostname, localhost
  default_transport: smtp
  mynetworks: 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
  mailbox_size_limit: '0'
  recipient_delimiter: '+'
  inet_interfaces: localhost
  inet_protocols: all
```

- `postfix_transports` - hash with the transport configuration

```
  google.com: smtp:127.0.0.1
```

- `postfix_aliases` - hash with the aliases

```
  root:
    - abuse
    - admin
  admin: root@local.loc
```

## How to use

```
- hosts: monitoring
  roles:
    - postfix_mta
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule molecule-docker docker ansible-lint pytest-testinfra yamllint`
- run `molecule test`

### Tests with docker

- install [docker](https://docs.docker.com/engine/installation/)
- install ruby
- install bundler `gem install bundler`
- install dependencies `bundle install`
- run the tests `kitchen test`

## License

```
Copyright (c) 2019 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
