import testinfra.utils.ansible_runner

def test_packages(host):
  for package_name in ['postfix', 'bsd-mailx']:
    package = host.package(package_name)
    assert package.is_installed

def test_config_file(host):
  path = host.file('/etc/postfix/main.cf')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644
  assert path.contains('inet_protocols = ipv4')

def test_transport_file(host):
  path = host.file('/etc/postfix/transport')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644
  assert path.contains('google.com smtp:127.0.0.1')

def test_aliases_file(host):
  path = host.file('/etc/postfix/aliases')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644
  assert path.contains('root: admin,abuse')

def test_service(host):
  service = host.service('postfix')
  assert service.is_running
  assert service.is_enabled

def test_socket(host):
  socket = host.socket('tcp://127.0.0.1:25')
  assert socket.is_listening
